import sys
import json
from jsonschema import validate

from gmailer.client import Gmailer

from secret_santa.assigner import SecretSantaAssigner, create_person
from secret_santa.messages import notify_people, remind_people


def load_parameters(parameters_file):
    """
    Load the input parameters from file.

    :param parameters_file: filepath to the input parameters
    :return: a JSON decoded dictionary of parameters
    """
    with open("secret_santa/input_schema.json", 'r') as input_schema:
        schema = json.load(input_schema)

        with open(parameters_file, 'r') as params:
            parameters = json.load(params)

    validate(parameters, schema)

    print "Validated input parameters..."

    return parameters


def run(parameters):
    """
    Run the assignments on the given parameters.

    :param parameters: a dictionary of parameters
    """
    # pull the parameters out first
    people_list = parameters["people_list"]
    sender_email = parameters["sender_email"]
    sender_password = parameters["sender_password"]

    # create the assigner instance
    assigner = SecretSantaAssigner(people_list)

    # run an assignment method to get a map
    print "Assigning people..."
    assignment_map = assigner.assign_by_shuffling()
    print "Assignments complete!"

    # create the gmailer instance
    gmailer = Gmailer(sender_email, sender_password)

    # send some emails!
    print "Notifying people..."
    notify_people(gmailer, assignment_map)
    print "Notification emails sent!"

    # wrap up
    print "*Wrapping* up..."
    print "All done! Merry Christmas!"


def run_reminder(parameters):
    """
    Run the assignments on the given parameters.

    :param parameters: a dictionary of parameters
    """
    # pull the parameters out first
    people_list = parameters["people_list"]
    sender_email = parameters["sender_email"]
    sender_password = parameters["sender_password"]

    people_list = [create_person(info) for info in people_list]

    # create the gmailer instance
    gmailer = Gmailer(sender_email, sender_password)

    # send some emails!
    print "Reminding people..."
    remind_people(gmailer, people_list)
    print "Reminder emails sent!"

    # wrap up
    print "*Wrapping* up..."
    print "All done! Merry Christmas!"


def main():
    """
    Load the input parameters and run the main assignment function.
    """
    parameters = load_parameters(sys.argv[1])
    run(parameters)


def main_reminder():
    """
    Load the input parameters and run the main reminder function.
    """
    parameters = load_parameters(sys.argv[1])
    run_reminder(parameters)


if __name__ == '__main__':
    main()
