from random import choice


JOKES = [
    "Q: What do you call Santa when he stops moving? A: Santa Pause!",
    "Q: What do you get if you cross mistletoe and a duck? A: A Christmas Quacker!",
    "Q: Why does everybody like Frosty the Snowman? A: Because he is so cool!",
    "Q: What do you call an obnoxious reindeer? A: RUDEolph",
    "Q: Why does Santa have three gardens? A: So he can 'ho ho ho'!",
    "Q: What do you get if you cross Santa with a duck? A: A Christmas Quacker!",
    "Q: Who delivers presents to baby sharks at Christmas? A: Santa Jaws",
    "Q: What says Oh Oh Oh? A: Santa walking backwards!",
    "Q: What goes Ho Ho Whoosh, Ho Ho Whoosh? A: Santa going through a revolving door!",
    "Q: Why does Santa go down the chimney on Christmas Eve? A: Because it 'soots' him!",
    "Q: Who is Santa's favorite singer? A: Elf-is Presley!",
    "Q: What do you call Santa's little helpers? A: Subordinate clauses!",
    "Q: What do Santa's little helpers learn at school? A: The elf-abet!",
    "Q: What did Santa say to the smoker? A: Please don't smoke, it's bad for my elf!",
    "Q: Where does Santa go when he's sick? A: To the elf centre!",
    "Q: Where do elves go to dance? A: Christmas Balls!",
    "Q: What do elves eat for breakfast? A: Frosted Flakes!",
    "Q: What do you call a frozen elf hanging from the ceiling? A: An elfcicle!",
    "Q: What type of Shoes does Santa wear when he travels on a train? A: Platforms!",
    "Q: What do you get if Santa goes down the chimney when a fire is lit? A: Krisp Kringle!",
    "Q: Who is Santa Claus married to? A: Mary Christmas!",
    "Q: How long do a reindeers legs have to be? A: Long enough so they can touch the ground!",
    "Q: What do reindeer hang on their Christmas trees? A: Horn-aments!",
    "Q: Why are Christmas trees so bad at sewing? A: They always drop their needles!",
    "Q: What's worse than Rudolph with a runny nose? A: Frosty the snowman with a hot flush!",
    "Q: Did Rudolph go to school? A: No. He was Elf-taught!",
    "Q: Why did the Rudolph cross the road? A: Because he was tied to the chicken!",
    "Q: What do you call Rudolph with lots of snow in his ears. A: Anything you want, he can't hear you!",
    "Q: What happened to the turkey at Christmas? A: It got gobbled!",
    "Q: Why did the turkey join the band? A: Because it had the drumsticks!",
    "Q: What do you get when you cross a snowman with a vampire? A: Frostbite!",
    "Q: What do snowmen wear on their heads? A: Ice caps!",
    "Q: How do snowmen get around? A: They ride an icicle",
    "Q: What do snowmen eat for lunch? A: Iceburgers!",
    "Q: What song do you sing at a snowman's birthday party? A: Freeze a jolly good fellow!",
    "Q: How does Good King Wenceslas like his pizzas? A: One that's deep pan, crisp and even!",
    "Q: Who hides in the bakery at Christmas? A: A mince spy!"
    "Q: What did Adam say on the day before Christmas? A: It's Christmas, Eve!",
    "Q: How many letters are in the angelic alphabet? A: The Christmas alphabet has 'no EL'!",
    "Q: What carol is heard in the desert? A: O camel ye faithful!",
    "Q: What do angry mice send to each other at Christmas? A: Cross Mouse Cards!"
]


def create_notification_message(person, assigned_person):
    """
    Create a personalised message notifiying a person of their assigned giftee.

    :param person: a person
    :param assigned_person: the assigned giftee for person
    :return: a string containing a notification message
    """
    message = "Ho ho ho!\n\n"
    message += "Seasons greetings {}!\n\n".format(person.name)
    message += "For this year's Secret Santa mission, you have been assigned: {}\n\n".format(assigned_person.name)
    message += "Here's a classic joke to get you into the Christmas spirit:\n\n"
    message += choice(JOKES)

    return message


def create_reminder_message(person):
    """
    Create a personalised message reminding a person to bring in their gift.

    :param person: a person
    :return: a string containing a notification message
    """
    message = "Ho ho ho!\n\n"
    message += "Seasons greetings {}!\n\n".format(person.name)
    message += "Today's the big day, don't forget to bring in your gift (or you'll make me look bad!)\n\n"
    message += "Here's one more classic joke to get you into the Christmas spirit:\n\n"
    message += choice(JOKES)

    return message


def notify_people(gmailer, assignment_map):
    """
    Send an email notification to each person in the assignment_map
    with their assigned giftee.

    :param gmailer: a Gmailer instance used to send the emails
    :param assignment_map: a dictionary of (person, assigned person) pairs
    """
    for person, assigned_person in sorted(assignment_map.iteritems(), key=lambda (person_a, person_b): person_a.name):
        print "Emailing {}...".format(person.name)
        message = create_notification_message(person, assigned_person)
        gmailer.send_mail(person.email, message, subject="Secret Santa!")


def remind_people(gmailer, people_list):
    """
    Send a reminder email to each person in the people_list.

    :param gmailer: a Gmailer instance used to send the emails
    :param assignment_map: a dictionary of (person, assigned person) pairs
    """
    for person in sorted(people_list, key=lambda person: person.name):
        print "Emailing {}...".format(person.name)
        message = create_reminder_message(person)
        gmailer.send_mail(person.email, message, subject="Secret Santa!")
