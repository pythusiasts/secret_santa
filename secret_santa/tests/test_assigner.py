import pytest

from secret_santa.assigner import SecretSantaAssigner


@pytest.fixture()
def four_name_assigner():
    assigner = SecretSantaAssigner([
        ("Alice", "alice@testmail.com"),
        ("Bob", "bob@testmail.com"),
        ("Charlie", "charlie@testmail.com"),
        ("Daniel", "daniel@testmail.com")
    ])

    return assigner


@pytest.mark.parametrize("name_email_list, expected_error", [
    ([("Nicholas", "nicholas@testmail.com")],
     "At least four people required! 1 given."),

    ([("Nicholas", "nicholas@testmail.com"), ("Klaus", "klaus@testmail.com")],
     "At least four people required! 2 given."),

    ([("Nicholas", "nicholas@testmail.com"), ("Klaus", "klaus@testmail.com"), ("Bro", "bro@testmail.com")],
     "At least four people required! 3 given.")
])
def test_assigner_at_least_one_name(name_email_list, expected_error):
    with pytest.raises(ValueError) as e:
        assigner = SecretSantaAssigner(name_email_list)
    assert e.value.message == expected_error


def test_assigner_invalid_email():
    with pytest.raises(ValueError) as e:
        assigner = SecretSantaAssigner([
            ("Notaname", "notanemail.com"),
            ("Alice", "alice@testmail.com"),
            ("Bob", "bob@testmail.com"),
            ("Charlie", "charlie@testmail.com")
        ])
    assert e.value.message == "Invalid email address supplied: \n['notanemail.com']"


def test_assigner_assign_by_shuffling(four_name_assigner):
    assigner = four_name_assigner
    assignment_map = assigner.assign_by_shuffling()

    assert assignment_map
    assert len(assignment_map) == 4

    # check people don't map to themselves
    assert all(assigned_person is not person for person, assigned_person in assignment_map.iteritems())

    # check input people match output people
    assert set(assigner.people) == set(assignment_map.keys()) == set(assignment_map.values())
