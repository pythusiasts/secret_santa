from random import randint, shuffle
from collections import deque, namedtuple


Person = namedtuple("Person", "name email")


class SecretSantaAssigner(object):
    def __init__(self, people_list):
        """
        A small class providing multiple methods for randomly assigning
        names to names under the Secret Santa game rules. Must have at
        least four names otherwise the assignments won't be secret.

        :param people_list: a list of (name, email) pairs to be assigned
        """
        self._verify_people_list(people_list)
        self.people = [create_person(info) for info in people_list]

    @staticmethod
    def _verify_people_list(people_list):
        """
        Verify there are at least four people given, raise ValueError if not.
        Otherwise the assignments will not be a secret! Also attempt to
        validate the email addresses.
        """
        if len(people_list) < 4:
            raise ValueError("At least four people required! {} given.".format(len(people_list)))

        invalid_emails = [info[1] for info in people_list if "@" not in info[1]]
        if invalid_emails:
            raise ValueError("Invalid email address supplied: \n{}".format(invalid_emails))

    def assign_by_shuffling(self):
        """
        Randomly assign people to people by shuffling and rotating.

        :return: a dictionary of (person, assigned person) pairs
        """
        # create a double ended queue which has a nice rotate method
        people_deque = deque(self.people)

        # shuffle to 'randomise' the assignments and keep a copy
        shuffle(people_deque)
        shuffled_people = deque(people_deque)

        # rotate by a random number to match people with people other than themselves
        rotation_num = randint(1, len(people_deque) - 1)
        people_deque.rotate(rotation_num)

        # create a dictionary of the assignments by zipping the shuffled deque with the rotated deque
        assignment_map = dict(zip(shuffled_people, people_deque))

        return assignment_map


def create_person(info):
    name, email = info
    return Person(name, email)
