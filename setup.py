from setuptools import setup, find_packages

version = '0.1.0'

requirements = [
    'pytest>=2.8.1',
]

entry_points = {
    'console_scripts': ['secret_santa = secret_santa.main:main'],
    'console_scripts': ['secret_santa_reminder = secret_santa.main:main_reminder']
}

setup(
    name='Secret Santa',
    version=version,
    description="Secret Santa assignment generator",
    author='Pythusiasts',
    author_email='pythusiasts@biarri.flowdock.com',
    packages=find_packages(exclude=['ez_setup', 'tests']),
    entry_points=entry_points,
    install_requires=requirements,
)
